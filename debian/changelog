gtkatlantic (0.6.3-2jk) unstable; urgency=medium

  * Non-maintainer upload.
  * Add patch to make the game board scalable

 -- Jan Karwowski <jan_karwowski@wp.pl>  Sat, 25 Jun 2022 16:01:42 +0200

gtkatlantic (0.6.3-1) unstable; urgency=medium

  * New upstream version 0.6.3.
    - Fix FTBFS with GCC-10. (Closes: #957316)
  * Declare compliance with Debian Policy 4.5.0.
  * Switch to debhelper-compat = 12.
  * Use canonical VCS URI.
  * Remove get-orig-source target.
  * Drop kfreebsd.patch. Fixed upstream.

 -- Markus Koschany <apo@debian.org>  Tue, 21 Apr 2020 01:18:24 +0200

gtkatlantic (0.6.2-2) unstable; urgency=medium

  * Add kfreebsd.patch and fix FTBFS on kfreebsd architectures.
    Thanks to Sylvain Rochet for the patch.
  * Remove dh-autoreconf from Build-Depends because we use compat level 10 now.

 -- Markus Koschany <apo@debian.org>  Sat, 24 Jun 2017 15:36:03 +0200

gtkatlantic (0.6.2-1) unstable; urgency=medium

  * New upstream version 0.6.2.
  * Declare compliance with Debian Policy 4.0.0.
  * Drop deprecated menu file.
  * Use https for Format field.

 -- Markus Koschany <apo@debian.org>  Fri, 23 Jun 2017 00:55:33 +0200

gtkatlantic (0.6.1-2) unstable; urgency=medium

  * Switch to compat level 10.
  * Declare compliance with Debian Policy 3.9.8.
  * Vcs-fields: Use https.
  * Update my e-mail address.

 -- Markus Koschany <apo@debian.org>  Mon, 23 Jan 2017 01:54:25 +0100

gtkatlantic (0.6.1-1) unstable; urgency=medium

  * Imported Upstream version 0.6.1.
  * get-orig-source: Remove --no-symlink option.
  * Add upstream's signing-key and verify new tarball releases.
  * Use upstream's new install mechanism for icons and drop links file.

 -- Markus Koschany <apo@gambaru.de>  Wed, 12 Aug 2015 11:44:24 +0200

gtkatlantic (0.6.0-1) unstable; urgency=medium

  * Imported Upstream version 0.6.0.
  * debian/rules: Add get-orig-source target.
  * Declare compliance with Debian Policy 3.9.6.
  * Vcs-Browser: Switch to cgit.
  * Update copyright years.
  * Install upstream's desktop file.
  * Install upstream's xpm icon.
  * Drop debian/icons because all high resolution icons
    are now also included in the source tarball.
  * Fix automatic installation of gtkatlantic.desktop file and install the file
    to /usr/share/applications.

 -- Markus Koschany <apo@gambaru.de>  Sat, 18 Jul 2015 12:52:56 +0200

gtkatlantic (0.5.0-2) unstable; urgency=medium

  * Install new high resolution icon to /usr/share/icons/hicolor/64x64/apps.

 -- Markus Koschany <apo@gambaru.de>  Sat, 05 Jul 2014 21:34:28 +0200

gtkatlantic (0.5.0-1) unstable; urgency=medium

  * Imported Upstream version 0.5.0.
    - Port to use GTK3.
  * debian/control:
    - Switch to libgtk-3-dev.
    - Drop libattr1-dev and libacl1 from Build-Depends. Not needed.
  * debian/rules:
    - Remove override for dh_auto_build.

 -- Markus Koschany <apo@gambaru.de>  Mon, 24 Mar 2014 20:15:32 +0100

gtkatlantic (0.4.4-1) unstable; urgency=medium

  * Imported Upstream version 0.4.4.
  * Update watch file.
  * Update copyright years.
  * Drop remove-Werror-from-CFLAGS.patch. Fixed upstream.
  * Drop hardening.patch. Add CFLAGS via debian/rules.
  * Export DEB_BUILD_MAINT_OPTIONS = hardening=+all.

 -- Markus Koschany <apo@gambaru.de>  Sun, 19 Jan 2014 15:41:08 +0100

gtkatlantic (0.4.3-1) unstable; urgency=medium

  * Imported Upstream version 0.4.3. (Closes: #652572)
  * Bump compat level to 9 and require debhelper >= 9.
  * Switch from build-dependency libpng12-dev to libpng-dev.
    (Closes: #662367)
  * Update homepage field and point to gtkatlantic.gradator.net.
  * Use canonical VCS-URI.
  * Bump Standards-Version and make the package comply with 3.9.5.
  * Switch to source format 3.0 (quilt).
  * Update the man pages for section 6 (games).
  * Update gtkatlantic.desktop file.
    - Add a comment in German.
    - Add keywords.
  * Update menu file.
    - Add longtitle.
    - Use an absolute icon path.
  * debian/watch: Make the extension regex more flexible.
  * Update package description and remove obsolete link. Add a
    trademark symbol for all occurrences of Monopoly. Thanks to Dan 'Da Man'.
    (Closes: #600158)
  * Install README via docs file.
  * Switch to dh sequencer.
    - Build with --parallel and --with autoreconf.
    - Add dh-autoreconf to Build-Depends.
    - Drop cdbs.
    - Avoid useless dependencies by appending -Wl,--as-needed to LDFLAGS.
    - Install game data to /usr/share/games.
  * Add remove-Werror-from-CFLAGS.patch. Do not treat warnings as errors to
    avoid build failures.
  * Add hardening.patch to add missing CFLAGS via dpkg-buildflags.
  * Update debian/copyright to copyright format 1.0.
  * Add myself to Uploaders.

 -- Markus Koschany <apo@gambaru.de>  Tue, 17 Dec 2013 18:01:29 +0100

gtkatlantic (0.4.2-3) unstable; urgency=low

  [ Barry deFreese ]
  * Change libglib1.2-dev build-dep to libglib2.0-dev.
  * Bump Standards Version to 3.8.1. (No changes needed).

 -- Barry deFreese <bdefreese@debian.org>  Sat, 11 Apr 2009 17:06:18 -0400

gtkatlantic (0.4.2-2) unstable; urgency=low

  [ Barry deFreese ]
  * New maintainer - Debian Games Team. (Closes: #497240).
    + Add myself to Uploaders.
  * Add VCS tags.
  * Move homepage to source stanza.
  * Move to section: games.
  * Bump Standards Version to 3.8.0. (No changes needed).

 -- Barry deFreese <bdefreese@debian.org>  Fri, 20 Feb 2009 15:38:26 -0500

gtkatlantic (0.4.2-1) unstable; urgency=low

  * New maintainer. (Closes: #428512).
  * ACK NMU. (Closes: #346678).
  * New upstream release.
  * debian/*: Use cdbs.
  * debian/copyright: Updated.
  * gtkatlantic.xpm: Removed duplicate icon file.
  * debian/rules, debian/menu: Install in /usr/games.
  * debian/watch: Added.

 -- Bart Martens <bartm@knars.be>  Fri, 15 Jun 2007 18:53:45 +0200

gtkatlantic (0.4.1-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Removed xlibs-dev build-dep. (Closes: #346678)

 -- Marc 'HE' Brockschmidt <he@debian.org>  Fri, 20 Jan 2006 22:22:29 +0100

gtkatlantic (0.4.1-1) unstable; urgency=low

  * New upstream release (Closes: #258201)

 -- Julien Delange <julien@gunnm.org>  Thu, 22 Jul 2004 15:30:43 +0200

gtkatlantic (0.3.2-1) unstable; urgency=low

  * New upstream release

 -- Julien Delange <julien@gunnm.org>  Mon,  5 Jan 2004 17:17:55 +0100

gtkatlantic (0.3.1-1) unstable; urgency=low

  * New upstream release

 -- Julien Delange <julien@gunnm.org>  Fri,  7 Nov 2003 19:44:50 +0100

gtkatlantic (0.3.0-1) unstable; urgency=low

  * New upstream release

 -- Julien Delange <julien@gunnm.org>  Fri, 31 Oct 2003 21:51:54 +0100

gtkatlantic (0.2.13-3) unstable; urgency=low

  * Put the right menu !
  * Add an icon in the menu
  * Add manpage for this program (gtkatlantic)

 -- Julien Delange <julien@gunnm.org>  Sun, 26 Oct 2003 18:04:11 +0100

gtkatlantic (0.2.13-2) unstable; urgency=low

  * Add a menuitem to the Debian menu

 -- Julien Delange <julien@gunnm.org>  Fri, 17 Oct 2003 14:46:56 +0200

gtkatlantic (0.2.13-1) unstable; urgency=low

  * Initial Release. (Closes: #212729)

 -- Julien Delange <julien@gunnm.org>  Thu, 25 Sep 2003 17:50:23 +0200
